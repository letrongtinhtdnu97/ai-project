# -*- coding: utf-8 -*-
"""
Nhập 1 câu string dài , tìm kiếm chữ hoa , chữ thường
"""

s = input("Nhập câu của bạn: ")
d={"UPPER CASE":0, "LOWER CASE":0}

for c in s:
    if c.isupper():
        d["UPPER CASE"]+=1 #kiểm tra chữ hoa
    elif c.islower():
        d["LOWER CASE"]+=1 #kiêm tra chữ thường
        
    else:
        pass
print ("Chữ hoa:", d["UPPER CASE"])
print ("Chữ thường:", d["LOWER CASE"])