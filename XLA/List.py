'''Bài toán nhập vào chuỗi số nguyên
 In ra danh sách đã sắp xếp theo chiều tăng dần,giảm dần, 
 giá trị lớn nhất, giá trị nhỏ của danh sách,
 tìm list số nguyên tố
 
 
 '''
 
from SortAndMaxMin import Nhap,SortASC,SortDES,MaxList,MinList,SNT

def main():
    list = Nhap()
    SortASC(list)
    SortDES(list)
    MaxList(list)
    MinList(list)
    listSNT = SNT(list)
    listSNT.sort()
    
    if listSNT == []:
        print("Không có SNT trong list")
    else:
        print("List so nguyen to la:",listSNT)
        
    

if __name__=="__main__":
    main()