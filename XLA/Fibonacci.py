'''Bài toán Fibonacci khi nhập vào n xuất ra dãy  Fibonacci'''

def Fibonacci(n):
   listFi = []
   f0, f1, fn = 0, 1, 0
   if n == 0:
      listFi.append(n)
   elif(n == 1):
        listFi.append(n)
   else:
       listFi=[0,1]
       for i in range(2,n):
           fn = f0 + f1
           f0 = f1
           f1 = fn
           listFi.append(fn)
   return listFi
    
def Nhap():
    n =-1
    while n < 0:
        n = int(input("Nhập vào số nguyên n:"))
        if(n > 0):
            break
    return n

def main():
    n = Nhap()
    listFi = Fibonacci(n)
    print("Day Fibonacci:",listFi)
    
if __name__=="__main__":
    main()