
"""
Các hàm dùng để import vào List.py
"""



def Nhap():
    values = input("Nhập chuỗi số nguyên(CÁC CHỮ CÁCH NHAU BẰNG DẤU (',')):")
    list = values.split(",")
    for i in range(0, len(list)): 
        list[i] = int(list[i]) 
    return list

def SortASC(list):
    for i in range(0,len(list)-1):
        k = i
        for j in range(i+1,len(list)):
            if(list[j] < list[k]):
                k = j
                Swap(list,i,k)
    print("List số nguyên tăng dần:", list)
    
def SortDES(list):
    for i in range(0,len(list)-1):
        k = i
        for j in range(i+1,len(list)):
            if(list[j] > list[k]):
                k = j
                Swap(list,i,k)
    print("List số nguyên giảm dần:", list)
    
def Swap(list, i ,k):
        temp = list[i]
        list[i] = list[k]
        list[k] = temp

'''list.sort()
    list.sort(reverse=True)
    print("List số nguyên giảm dần",list)'''

def MaxList(list):
    Max = list[0]
    for x in range(0,len(list)):
        if(Max < list[x]):
            Max = list[x]
    print("Giá trị lớn nhất của List: ", Max)

def MinList(list):
    Min = list[0]
    for y in range(0,len(list),1):
        if(Min > list[y]):
            Min = list[y]
    print("Giá trị nhỏ nhất của List: ",Min)


def SNT(list):
    listSNT = []
    for snt in list:
        if(KtSNT(snt) == True):
            listSNT.append(snt)
    return listSNT

def KtSNT(snt):
    if(snt < 2):
        return False
    for x in range(2,snt-1):
        if(snt % x == 0):
            return False
    return True
        
