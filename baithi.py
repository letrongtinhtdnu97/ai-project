# -*- coding: utf-8 -*-
"""
1.Median
2.Mean
3.Histogram
4.Biến đổi đen trắng
5.Negative
"""
import os
from tkinter import Frame, Tk, BOTH, Menu,Label,Button,Entry
from tkinter.filedialog import Open
from PIL import Image,ImageTk
import numpy
import cv2 
image = Image.NONE

def onOpen():     
        ftypes = [('Python files', '*.*'), ('All files', '*')]
        dlg = Open(filetypes = ftypes)
        global fl
        fl = dlg.show()
        
        if fl != '':
            im = readFile(fl)
            show(im)
            global image
            image = im
                        
            
def readFile(filename):
        im = Image.open(filename)
        return im
    
def show(im):
    im2 = ImageTk.PhotoImage(im)
    label1 = Label(frame,height=400,width=300,image=im2)
    label1.image = im2
    label1.place(x=10, y=250)
#loc median
def median():
        global image
        
        a = 3
        if (entry4.get() != ""):
          a = int(entry4.get())
        im2 = image.convert("L")
        arr = numpy.array(im2)
        removed_noise = median_filter(arr, a)
        im2 = Image.fromarray(removed_noise)
        im2 = ImageTk.PhotoImage(im2)
        label2 = Label(frame,height=400,width=300,image=im2)
        label2.image = im2
        label2.place(x=500, y=250)
        
def median_filter(data, filter_size):
          temp = []
          indexer = filter_size // 2
          data_final = []
          data_final = numpy.zeros((len(data),len(data[0])))
          for i in range(len(data)):
            for j in range(len(data[0])):
             for z in range(filter_size):
                if i + z - indexer < 0 or i + z - indexer > len(data) - 1:
                    for c in range(filter_size):
                        temp.append(0)
                else:
                    if j + z - indexer < 0 or j + indexer > len(data[0]) - 1:
                        temp.append(0)
                    else:
                        for k in range(filter_size):
                            temp.append(data[i + z - indexer][j + k - indexer])

             temp.sort()
             data_final[i][j] = temp[len(temp) // 2]
             temp = []
          return data_final;
      
def mean():
    global image
    
    a = 3
    
    if (entry4.get() != ""):
     a = int(entry4.get())
    im = image.convert("L")
    im = numpy.array(image)
    im = mean_filter(im,a)
    im = Image.fromarray(im)
    im = ImageTk.PhotoImage(im)
    label = Label(frame,height=400,width=300,image=im)
    label.image = im
    label.place(x=900, y=250)
    
    
def mean_filter(data, filter_size):
          temp = []
          mask = numpy.ones((filter_size**2))/(filter_size**2)
          indexer = filter_size // 2
          data_final = []
          data_final = numpy.zeros((len(data),len(data[0])))
          for i in range(len(data)):
            for j in range(len(data[0])):
             for z in range(filter_size):
                if i + z - indexer < 0 or i + z - indexer > len(data) - 1:
                    for c in range(filter_size):
                        temp.append(0)
                else:
                    if j + z - indexer < 0 or j + indexer > len(data[0]) - 1:
                        temp.append(0)
                    else:
                        for k in range(filter_size):
                            temp.append(data[i + z - indexer][j + k - indexer])
             d = 0
             for z in range(len(temp)):
              d = d + temp[z]*mask[z]
             data_final[i][j] = d
             temp = []
          return data_final;
    

def save_histo_GramsEqua1():
    global image
    
    image.save('imageHis/anh1_1.png')
    image.save('imageMean/anh1_1.png')
    image.save('imageMedian/anh1_1.png')
    
    #opencvImage = cv2.cvtColor(numpy.array(pil_image), cv2.COLOR_RGB2BGR)
def show_histo_1():
    histo_GramsEqua1()
    '''
    cdf_m = np.ma.masked_equal(cdf,0)
    cdf_m = (cdf_m - cdf_m.min())*255/(cdf_m.max()-cdf_m.min())
    cdf = np.ma.filled(cdf_m,0).astype('uint8')
    '''
   
    
    im = Image.open('imageHis/anh1_2.png')
    im = ImageTk.PhotoImage(im)
    label = Label(frame,height=400,width=300,image=im)
    label.image = im
    label.place(x=1300, y=250)
   
        
def histo_GramsEqua1():
    
    img = cv2.imread('imageHis/anh1_1.png',0)
    equ = cv2.equalizeHist(img)
    cv2.imwrite('imageHis/anh1_2.png',equ)
def xoaAnh():
    os.remove('imageThreshold/anh1_2.png')
def xoaAnh1():
    os.remove('imageNegative/anh1_2.png')
    
def saveMau():
    global image
    image.save('imageThreshold/anh1_1.png')
    
def biendoi_negativeTrans_Amban():
    a=255
    if (entry3.get() != ""):#nhap gia trị L
        a = int(entry3.get())
        
    img = cv2.imread("imageNegative/anh1_1.png")
    
    img2 = a - img
    cv2.imwrite('imageNegative/anh1_2.png',img2)
    im = Image.open('imageNegative/anh1_2.png')
    im = ImageTk.PhotoImage(im)
    label = Label(frame,height=400,width=300,image=im)
    label.image = im
    label.place(x=1300, y=250)
    label6.config(text="Biến đổi âm bản(Image negative transformation)")
     
   
def biendoi_dentrang():
    a = 125
    b = 125
    if (entry3.get() != ""):
     a = int(entry3.get())
    if(entry2.get() !=""):
        b = int(entry2.get())     
    img2 = cv2.imread("imageThreshold/anh1_1.png")
    img2_gray = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
    _, mask = cv2.threshold(img2_gray, a, b, 0)
    cv2.imwrite('imageThreshold/anh1_2.png',mask)
    
    im = Image.open('imageThreshold/anh1_2.png')
    im = ImageTk.PhotoImage(im)
    label = Label(frame,height=400,width=300,image=im)
    label.image = im
    label.place(x=1300, y=250)
    label7.config(text="Biến đổi ảnh xám")
#Viet huong dan su dung
def huongdansudung():
    root1 = Tk()
    root1.title("Hướng dẫn sử dụng")
    frame1 = Frame(root1)
    frame1.pack(fill=BOTH, expand=1)
    root1.geometry("1880x900")
    root1.mainloop()
#viet gioi thieu 
def gioiThieu_members():
    root2 = Tk()
    root2.title("Giới thiệu thành viên nhóm")
    frame2 = Frame(root2)
    frame2.pack(fill=BOTH, expand=1)
    root2.geometry("1880x900")
    root2.mainloop()   
def main():
 root = Tk()
 root.title("Thi")
 global frame
 frame = Frame(root)
 frame.pack(fill=BOTH, expand=1)
 menubar = Menu(root)
 root.config(menu=menubar)
 fileMenu = Menu(menubar)
 menubar.add_cascade(label="File", menu=fileMenu)
 fileMenu.add_command(label="Open", command=onOpen)
 fileMenu.add_command(label="Close", command=root.destroy)
 label = Label(frame,text = "Size")
 label.place(x=10,y=700)
 
 #entry 3
 label11 = Label(frame,text = "Pixel y")
 label11.place(x=10,y=750) #
 global entry
 global entry2
 global entry3 
 global entry4
 entry = Entry(frame)
 entry.place(x=120,y=850)
 entry4 = Entry(frame)
 entry4.place(x=70,y=700)
 entry3 = Entry(frame)
 entry3.place(x=70,y=750)
 
 #entry2
 label0 = Label(frame,text = "Pixel x")
 label0.place(x=10,y=800)
 
 #entry
 label12 = Label(frame,text = "Threshold(ngưỡng)")
 label12.place(x=10,y=850)
 
 entry2 = Entry(frame)
 entry2.place(x=70,y=800)
 button = Button(frame,text = "Lọc median",command=median)
 button.place(x=520,y=700)
 button1 = Button(frame,text = "Lọc mean(average)",command=mean)
 button1.place(x=920,y=700)
 button44 = Button(frame,text = "Negative",command=biendoi_negativeTrans_Amban)
 button44.place(x=920,y=750)
 button45 = Button(frame,text = "Del Nagetive",command=xoaAnh1)
 button45.place(x=920,y=790)
 #button2 = Button(frame,text = "Lọc gaussian",command=gaussian)histo_GramsEqua
 button2 = Button(frame,text = "Histograms",command=show_histo_1)
 button2.place(x=1320,y=700)
 button3 = Button(frame,text = "Lưu ảnh xám",command=save_histo_GramsEqua1)
 button3.place(x=1450,y=700)
 button4 = Button(frame,text = "Lưu ảnh màu",command=saveMau)
 button4.place(x=1550,y=700) 
 button5 = Button(frame,text = "Grayscale",command=biendoi_dentrang)
 button5.place(x=1650,y=700)
 button6 = Button(frame,text = "Del Grayscale",command=xoaAnh)
 button6.place(x=1730,y=700)
 button7 = Button(frame,text = "Giới thiệu nhóm",command=xoaAnh)
 button7.place(x=1650,y=10)
 button8 = Button(frame,text = "Hướng dẫn sử dụng",command=huongdansudung)
 button8.place(x=1650,y=40)
 label1 = Label(frame,text = "NÂNG CAO CHẤT LƯỢNG ẢNH BẰNG CÁC PHƯƠNG PHÁP",fg = "blue",font = "Times 38 bold")
 label1.place(x=50,y=10)
 label2 = Label(frame,text = "Thành viên: Lê Trọng Tình , Trần Đình Đạt ",font="Times 16 bold")
 label2.place(x=450,y=100)
 label3 = Label(frame,text = "Ảnh gốc",fg = "red",font="Times 16 bold")
 label3.place(x=30,y=150)
 label4 = Label(frame,text = "Kết quả",fg = "red",font="Times 16 bold")
 label4.place(x=730,y=150)
 label5 = Label(frame,text = "Median",fg = "red",font="Times 14 bold")
 label5.place(x=550,y=200)
 global label6
 label6 = Label(frame,text = "Mean(Average)",fg = "red",font="Times 14 bold")
 label6.place(x=950,y=200)
 global label7
 
 label7 = Label(frame,text = "Histogram Equalization(Cân bằng Histogram )",fg = "red",font="Times 14 bold")
 label7.place(x=1300,y=200)
 root.geometry("1880x900")
 root.mainloop()

main()